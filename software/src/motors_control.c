
#include "motors_control.h"

void setup_PWM_timer(void) {  //IMPORTA EL ORDEN

 // __CONFIGURACIÓN DEl MICRO__ //
  //Set clok velocity 72mhz
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
  // activate the cloks
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_GPIOA);
  // Declare the pwm (Timer4, channel 1 PB6)right motor
  gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_TIM4_CH1);
  // Declare the pwm (Timer1, channel 3 PA10) left motor
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL , GPIO_TIM1_CH3);
  // Start the pwm clocks
  rcc_periph_clock_enable(RCC_TIM4);
  rcc_periph_clock_enable(RCC_TIM1);

//Configure tiemr 1 - LEFT MOTOR
  timer_set_mode(TIM1, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  timer_set_prescaler(TIM1, 25); // 571 Hz of PWM
  timer_set_repetition_counter(TIM1, 0);
  timer_enable_preload(TIM1);
  timer_continuous_mode(TIM1);
  timer_set_period(TIM1, 4500);

//Configure timer 4 - RIGHT MOTOR
  timer_set_mode(TIM4, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  timer_set_prescaler(TIM4, 25); // 571 Hz of PWM
  timer_set_repetition_counter(TIM4, 0);
  timer_enable_preload(TIM4);
  timer_continuous_mode(TIM4);
  timer_set_period(TIM4, 4500);

//configuracion de las salidas del timer que se van a usar y precargas 
  timer_set_oc_mode(TIM4, TIM_OC1, TIM_OCM_PWM1);
// The linefollower starts sttoped
  timer_set_oc_value(TIM4, TIM_OC1, 0);
// Set the timer exit
  timer_enable_oc_output(TIM4, TIM_OC1);

  timer_set_oc_mode(TIM1, TIM_OC3, TIM_OCM_PWM1);
// The linefollower starts sttoped
  timer_set_oc_value(TIM1, TIM_OC3, 0);
// Set the timer exit
  timer_enable_oc_output(TIM1, TIM_OC3);

  timer_enable_break_main_output(TIM1);

//Enable timers
  timer_enable_counter(TIM1);
  timer_enable_counter(TIM4);
  
}

void setup_tb6612fng(){
    // PINS TB6612FNG
    gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO12 | GPIO11);
    gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO4 | GPIO3);

// CONFIGURATION TB6612FNG IN ADVACE
    gpio_clear(GPIOA, GPIO12);
    gpio_set(GPIOA, GPIO11);
    gpio_set(GPIOB, GPIO4);
    gpio_clear(GPIOB, GPIO3);

//  ACTIVATE OPERATION TB6612FNG
    gpio_set(GPIOA, GPIO15);
}