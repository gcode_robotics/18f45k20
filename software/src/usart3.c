#include <usart3.h>

void usart3_setup(void)
{
	// Enable clocks for GPIO port B (for GPIO_USART3_TX) and USART3
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_USART3);

	// Setup GPIO pin GPIO_USART3_TX
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART3_TX);
	gpio_set_mode(GPIOB, GPIO_MODE_INPUT,GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_USART3_RX);

	// Setup USART parameters. 
	usart_set_baudrate(USART3, 9600);
	usart_set_databits(USART3, 16	);
	usart_set_stopbits(USART3, USART_STOPBITS_1);
	usart_set_mode(USART3, USART_MODE_TX_RX);
	usart_set_parity(USART3, USART_PARITY_NONE);
	usart_set_flow_control(USART3, USART_FLOWCONTROL_NONE);

	// Finally enable the USART. 
	usart_enable(USART3);
	
}

void my_usart_print_int(uint32_t usart, int value)
//The function usart_send_blocking works char by char
//This function converts the integer to a char matix and sends
//the elements sequentially
{
	int8_t i;
	uint8_t nr_digits = 0;
	char buffer[25];

	//If the integer is a negative number a '-' sign is sent and the value is
	//converted to positive
	if (value < 0) {
		usart_send_blocking(usart, '-');
		value = value * -1;
	}

	//The value is translated to the char matrix
	while (value > 0) {
		//The last number (units) of the value is saved in the char matrix
		//It is calculated as the remainder (mod %) dividing by 10
		//The index of the matrix is post-incremented
		buffer[nr_digits++] = "0123456789"[value % 10];
		//The value is divided by 10 to find the next number
		value /= 10;
	}

	for (i = (nr_digits - 1); i >= 0; i--) {
		//The values are sent last to first so that the value is read left to right
		usart_send_blocking(usart, buffer[i]);
	}

	usart_send_blocking(usart, '\r');
}
