#include "adc_dma.h"

void adc_setup(void)
{
	int i;
	
  // This ADC configuration has to be inizialiced by software
  // to convert a secuente of channels automaticaly. 
  // This ADC it is syncronize with the DMA, wich means that the 
  // DMA write the converted values on a memory address asigned

	// Enable ADC1 Clock
	rcc_periph_clock_enable(RCC_ADC1);

	// Make sure the ADC doesn't run during config
	adc_off(ADC1);

	// We configure everything for one scan conversion
	adc_enable_scan_mode(ADC1);
	adc_set_continuous_conversion_mode(ADC1);
	adc_disable_external_trigger_regular(ADC1);
	adc_set_right_aligned(ADC1);

	adc_set_sample_time_on_all_channels(ADC1, ADC_SMPR_SMP_1DOT5CYC);
	adc_power_on(ADC1);

	// Wait for ADC starting up
	for (i = 0; i < 800000; i++)    // Wait a bit
		__asm__("nop");

	adc_reset_calibration(ADC1);
	adc_calibration(ADC1);

	// Channels to scan : IR matrix
	uint8_t channels[NUM_SENSORS] = {ADC_CHANNEL0, ADC_CHANNEL1, 
  ADC_CHANNEL2, ADC_CHANNEL3, ADC_CHANNEL4, 
  ADC_CHANNEL5, ADC_CHANNEL6, ADC_CHANNEL7};
	// Sets the scanning sequence 
	adc_set_regular_sequence(ADC1, NUM_SENSORS, channels);


}

void setup_dma_adc1(void) {
  // Enables DMA1 Clock
  rcc_periph_clock_enable(RCC_DMA1);
  // Resets the channel and clears the registers before configuration
  dma_channel_reset(DMA1, DMA_CHANNEL1);
  // Sets the ADC peripheral address
  // (uint32_t)&ADC_DR(ADC1) represents the ADC1 address 
  dma_set_peripheral_address(DMA1, DMA_CHANNEL1, (uint32_t)&ADC_DR(ADC1));
  // Sets the base memory address assigned to the peripheral's channel
  // where you want to take the values
  dma_set_memory_address(DMA1, DMA_CHANNEL1, (uint32_t)&sensors);
  // Enables the memory to increment its pointer according to the data size
  // after each write operation
  // This does not change the base memory address
  dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL1);
  // Peripheral data size (8, 16, 32) (word size)
  dma_set_peripheral_size(DMA1, DMA_CHANNEL1, DMA_CCR_PSIZE_16BIT);
  // Memory data size (8, 16, 32) (word size)
  dma_set_memory_size(DMA1, DMA_CHANNEL1, DMA_CCR_MSIZE_16BIT);
  // Channel priority from LOW to VERY_HIGH
  dma_set_priority(DMA1, DMA_CHANNEL1, DMA_CCR_PL_HIGH);

  // Sets the size of the memory block in words
  dma_set_number_of_data(DMA1, DMA_CHANNEL1, NUM_SENSORS);
  // Enables the dma to write over the first word in the base 
  // memory address after it has reached the end of the memory block
  dma_enable_circular_mode(DMA1, DMA_CHANNEL1);
  // Sets transfer direction peripheral to memory
  dma_set_read_from_peripheral(DMA1, DMA_CHANNEL1);
  // Associates the channel to the stream
  // Enables the stream
  dma_enable_channel(DMA1, DMA_CHANNEL1);
  // Enables dma transfers
  adc_enable_dma(ADC1);
}

// This function calculate an averange over the maximum and minimum
// value of the IR matrix, in order to take a trheshold used to
// digitalize the values.
void ir_matrix_calibration(void){

    for(int j = 0; j < NUM_SAMPLES; j++){
      for(int i = 0; i < 8; i++){
        if(sensors[i] > max_average){
          max_average = sensors[i];
        }else if(sensors[i] < min_average){
          min_average = sensors[i];
        }
      }
    }
  first_threshold = (max_average + min_average)/2;

  for(int j = 0; j < NUM_SAMPLES; j++){

      // calculate the averege of the sensors
      for(int i = 0; i < NUM_SENSORS; i++){
        if(sensors[i] < first_threshold){ 
          partial_sum += sensors[i];
          num_under_threshold++;
        }
      }
        partial_average = partial_sum / num_under_threshold;
        num_under_threshold = 0;

      // calculate the typical deviation
      for(int i = 0; i < NUM_SENSORS; i++){
        if(sensors[i] < first_threshold){ 
          typical_deviation_sum = (sensors[i] - partial_average) ^ 2;
          num_under_threshold++;
        }
      }
        typical_deviation = sqrt(typical_deviation_sum / num_under_threshold);
        total_sum += partial_average + 3 * typical_deviation;
      
  }
  digital_threshold  = total_sum / NUM_SAMPLES;
}


