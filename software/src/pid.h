#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/timer.h>
#include <stdio.h>
#include "config.h"
#include "adc_dma.h"
#include "usart3.h"

#define kp 170
#define kd 300
static int16_t linefollower_position = 0;
static int16_t linefollower_position_prev = 0;
static int16_t derivative = 0;
static int16_t matriz_ponderaciones[NUM_SENSORS] = {-7, -5, -3, -1, 1, 3, 5, 7};
static uint8_t num_sensors_over_line;
static int16_t actuacion;
static int16_t vbase = 3500;   // hasta 4800
static int16_t v_left = 0;
static int16_t v_right = 0;

void pid();