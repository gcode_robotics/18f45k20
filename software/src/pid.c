#include "pid.h"

void pid(){
	
	linefollower_position = 0;
	num_sensors_over_line = 0;
	for(int i = 0; i < NUM_SENSORS; i++){
		if(sensors[i] >= digital_threshold){   // black line means > threshold
		linefollower_position += matriz_ponderaciones[i];
		num_sensors_over_line++;   // number of sensors over the line
		}
	}

	if(num_sensors_over_line == 1){ 
		linefollower_position *= 2;
	}

	// Aplicate PD
	derivative = linefollower_position - linefollower_position_prev;
	linefollower_position_prev = linefollower_position;
	actuacion = linefollower_position * kp + derivative * kd;

	// Make sure the actuation is between 0 and 4800 (maximo duty cycle of PWM)
	if (vbase - actuacion < 0){
		v_left = 0;
	}else if(vbase -actuacion > 4800){
		v_left = 4800;
	}else{
		v_left = vbase - actuacion;
	}

	if (vbase + actuacion > 4800){
		v_right = 4800;
	}else if(vbase + actuacion < 0){
		v_right = 0;
	}else{
		v_right = vbase + actuacion;
	}
	// Update motor actuation
	timer_set_oc_value(TIM1, TIM_OC3, v_left);   
    timer_set_oc_value(TIM4, TIM_OC1, v_right);

  for (int i = 0; i < NUM_SENSORS; i++){ 
        my_usart_print_int(USART3, sensors[i]);
  }

		usart_send_blocking(USART3, 'd');
		my_usart_print_int(USART3, digital_threshold);
		usart_send_blocking(USART3, 'p');
		my_usart_print_int(USART3, linefollower_position);
		usart_send_blocking(USART3, 'a');
		my_usart_print_int(USART3, actuacion);
		usart_send_blocking(USART3, 'l');
		my_usart_print_int(USART3, v_left);
		usart_send_blocking(USART3, 'r');
		my_usart_print_int(USART3, v_right);
		usart_send_blocking(USART3, '\n');

}