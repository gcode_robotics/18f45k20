#include "config.h"
#include "delay.h"
#include "motors_control.h"
#include "pid.h"

void initial_routine(){
  //  if(!gpio_port_read(GPIOB)){   //switches to select speed
        
  //  }
	// wait a bit in order to prove the linefollower its well located
  	for (int i = 0; i < 40000000; i++) {	/* Wait a bit. */
			__asm__("nop");
		}
	// led on
    gpio_set(GPIOB, GPIO8);
	ir_matrix_calibration();
	// led off
    gpio_clear(GPIOB, GPIO8);
	// wait for initial switch
    while(!(gpio_port_read(GPIOB) & GPIO12));   //pin read   

	// wait 5 seconds because normative
    for (int i=0; i<=14400000; i++){
      __asm__("nop");
    }

    // delay(5000);
}

int main(void){

    gpio_setup();
    usart3_setup();
    setup_PWM_timer();
    setup_tb6612fng();
  	adc_setup();
  	setup_dma_adc1();
    //START THE ADC SECUENCE CONVERSION
	  adc_start_conversion_direct(ADC1);
    
    
  	initial_routine();

    while(1){
        pid(); //linefollower

    }
    return 0;
}