#include <stdio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/adc.h>
#include "config.h"

#define NUM_SAMPLES 10

volatile uint16_t sensors[NUM_SENSORS];

static uint16_t first_threshold, max_average = 0, min_average = 4000, num_under_threshold = 0;
static uint16_t partial_sum, partial_average, typical_deviation_sum, typical_deviation, total_sum;
uint16_t digital_threshold;

void setup_dma_adc1(void);
void adc_setup(void);
void ir_matrix_calibration(void);

