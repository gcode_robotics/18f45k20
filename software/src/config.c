#include "config.h"

#define NUM_SENSORS 8

void gpio_setup(void)
{
	/* Enable GPIO clocks. */
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOC);
  	rcc_periph_clock_enable(RCC_GPIOB);

  // LED
  gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO8);

  // SWITCHES
  gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO12 | GPIO13 | GPIO14 | GPIO15);
  GPIO_ODR(GPIOB) = GPIO12 | GPIO13 | GPIO14 | GPIO15; // HABILITAR EL PULL-UP
}

 