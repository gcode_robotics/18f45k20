#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>

void setup_PWM_timer(void) {  //IMPORTA EL ORDEN

///////////////////////////PRIMERO/////////////////////////
 // __CONFIGURACIÓN DEl MICRO__ //
  // Establece la velocidad del reloj en 72mhz
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
    //activa los clocks
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_GPIOA);
  // Establece el pin GPIO_TIM4_CH4 (PB9) y el timer 1_ch3 (PA10) como salida PWM. 
  gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_TIM4_CH1);
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL , GPIO_TIM1_CH3);
  // Inicia los relojes del TIM4 como periferico
  rcc_periph_clock_enable(RCC_TIM4);
  rcc_periph_clock_enable(RCC_TIM1);

///////////////////////////SEGUNDO/////////////////////////
//preparacion del tiemr 1 - LEFT MOTOR
timer_set_mode(TIM1, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  timer_set_prescaler(TIM1, 25); // 571 Hz 
  timer_set_repetition_counter(TIM1, 0);
  timer_enable_preload(TIM1);
  timer_continuous_mode(TIM1);
  timer_set_period(TIM1, 4500);

  timer_set_mode(TIM4, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
//preparacion timer 4 - RIGHT MOTOR
  timer_set_prescaler(TIM4, 25); // 571 Hz
  timer_set_repetition_counter(TIM4, 0);
  timer_enable_preload(TIM4);
  timer_continuous_mode(TIM4);
  timer_set_period(TIM4, 4500);

//configuracion de las salidas del timer que se van a usar y precargas 
  timer_set_oc_mode(TIM4, TIM_OC1, TIM_OCM_PWM1);
  timer_set_oc_value(TIM4, TIM_OC1, 4500);
  timer_enable_oc_output(TIM4, TIM_OC1);


  timer_set_oc_mode(TIM1, TIM_OC3, TIM_OCM_PWM1);
  timer_set_oc_value(TIM1, TIM_OC3, 4500);
  timer_enable_oc_output(TIM1, TIM_OC3);
//esto es para timers 1 y 2, y los complejos del 3 (el chanel 3 PA6 va sin el)
  timer_enable_break_main_output(TIM1);

//enable que conten los timers
  timer_enable_counter(TIM1);
  timer_enable_counter(TIM4);
  
}

void setup_tb6612fng(){
    // DECLARACIÓN DE LOS PINES DEL TB6612FNG
    gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO12 | GPIO11);
    gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO4 | GPIO3);

// CONFIGURACIÓN DEL TB6612FNG EN AVANCE
    gpio_clear(GPIOA, GPIO12);
    gpio_set(GPIOA, GPIO11);
    gpio_set(GPIOB, GPIO4);
    gpio_clear(GPIOB, GPIO3);

// ACTIVACIÓN DEL FUNCIONAMIENTO DEL TB6612FNG
    gpio_set(GPIOA, GPIO15);
}

int main (void){
    setup_PWM_timer();
    setup_tb6612fng();


    int velocidad_iz=2400; //pongo 0 para indicar el incremento que no exceda los 4800 ojo
    int velocidad_der=2400; 
    timer_set_oc_value(TIM1, TIM_OC3, velocidad_iz);   // duty entre 0 y 4800
    timer_set_oc_value(TIM4, TIM_OC1, velocidad_der);

    while(1);
         

return 0;
}